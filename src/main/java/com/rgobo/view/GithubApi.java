package com.rgobo.view;

import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;

@Named
@ViewScoped
public class GithubApi implements Serializable {

    private final String url = "https://api.github.com/users/mralexgray/repos";


    public void onLoad() {
//        try {
//            JSONObject jo = readJsonFromUrl(url);
//            String title = jo.getString("");
//            String id = jo.getString("id");
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }


    private String readUrl(String urlString) throws Exception {
        BufferedReader reader = null;
        try {
            URL url = new URL(urlString);
            reader = new BufferedReader(new InputStreamReader(url.openStream()));
            StringBuffer buffer = new StringBuffer();
            int read;
            char[] chars = new char[1024];
            while ((read = reader.read(chars)) != -1)
                buffer.append(chars, 0, read);

            return buffer.toString();
        } finally {
            if (reader != null)
                reader.close();
        }
    }

    private String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        InputStream is = new URL(url).openStream();
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
//            JSONArray json = new JSONObject(jsonText);
//            return json;
            return null;
        } finally {
            is.close();
        }

    }
}
