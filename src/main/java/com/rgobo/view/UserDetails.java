package com.rgobo.view;

import com.rgobo.model.User;
import com.rgobo.controller.UserManager;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@ViewScoped
public class UserDetails implements Serializable {

    @Inject
    private UserManager userManager;

    private User user;

    public String submit() {
        return userManager.save(user);
    }

    public void onLoad() {
        user = userManager.isSignedIn() ? userManager.getCurrentUser() : new User();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
