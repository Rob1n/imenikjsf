package com.rgobo.view;

import com.rgobo.controller.UserManager;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

@Named
@RequestScoped
public class Login implements Serializable {

    @Inject
    private UserManager userManager;

    private String username;
    private String password;

    public void submit(){
        userManager.signIn(username, password);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
