package com.rgobo.view;


import com.rgobo.service.FileUService;

import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import java.io.Serializable;
import java.util.logging.Logger;

@Named
@RequestScoped
public class FileUController implements Serializable {
    private final Logger log = Logger.getLogger(getClass().getName());

    private Part file;
    private String fileContent;

    private static final String uploads = "";

    @Inject
    private FileUService fileUService;

    public void upload() {

//        if (file == null) {
//            log.warning("file empty");
//            System.out.println("File empty");
//            return;
//        }
//
//        String filename = "";
//        try (InputStream input = file.getInputStream()) {
//            filename = file.getName();
//            Files.copy(input, new File(uploads, filename).toPath());
//        } catch (IOException e) {
//            // Show faces message?
//            log.warning("error");
//        }

//        FileU newFile = new FileU(filename, uploads);
//        fileUService.createFileU(newFile);


//        try {
//            fileContent = new Scanner(file.getInputStream())
//                    .useDelimiter("\\A").next();
//        } catch (IOException e) {
//            // Error handling
//            log.warning("error");
//        }
//        log.info(fileContent);
    }

    public void validateFile(FacesContext ctx, UIComponent comp, Object value) {

//        List<FacesMessage> msgs = new ArrayList<FacesMessage>();
//
//        if (file == null) {
//            msgs.add(new FacesMessage("no file selected"));
//            throw new ValidatorException(msgs);
//        }
//
//        Part file = (Part) value;
//        if (file.getSize() > 1024) {
//            msgs.add(new FacesMessage("file too big"));
//        }
//        if (!"text/plain".equals(file.getContentType())) {
//            msgs.add(new FacesMessage("not a text file"));
//        }
//        if (!msgs.isEmpty()) {
//            throw new ValidatorException(msgs);
//        }
    }

    public Part getFile() {
        return file;
    }

    public void setFile(Part file) {
        this.file = file;
    }

    public String getFileContent() {
        return fileContent;
    }

    public void setFileContent(String fileContent) {
        this.fileContent = fileContent;
    }
}
