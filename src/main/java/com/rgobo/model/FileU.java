package com.rgobo.model;

import javax.persistence.*;

@Entity
@Table(name = "files")
public class FileU {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String path;
//
//    public FileU(){}
//
//    public FileU(String name, String path) {
//        this.name = name;
//        this.path = path;
//    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
