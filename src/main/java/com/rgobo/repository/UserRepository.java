package com.rgobo.repository;

import com.rgobo.model.User;

import java.util.List;

public interface UserRepository  {

    User saveUser(User user);

    List<User> getAllUsers();

    User getUser(Long id);

    User getUserByUsername(String username);
}
