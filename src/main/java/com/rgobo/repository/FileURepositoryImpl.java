package com.rgobo.repository;

import com.rgobo.model.FileU;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
@Transactional
public class FileURepositoryImpl implements FileURepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public FileU getFileU(Long id){
        return em.find(FileU.class, id);
    }

    @Override
    public List<FileU> getAllFileUs(){
        List<FileU> files = new ArrayList<>();
        Query query = em.createQuery("SELECT f FROM FileU f");
        files = new ArrayList<FileU>(query.getResultList());
        return files;
    }

    @Override
    public FileU createFileU(FileU file){
        try {
            em.persist(file);
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
        return file;
    }




}
