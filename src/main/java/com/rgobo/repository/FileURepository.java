package com.rgobo.repository;

import com.rgobo.model.FileU;

import java.util.List;

public interface FileURepository {


    FileU getFileU(Long id);

    List<FileU> getAllFileUs();

    FileU createFileU(FileU file);
}
