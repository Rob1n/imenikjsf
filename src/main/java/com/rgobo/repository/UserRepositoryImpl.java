package com.rgobo.repository;

import com.rgobo.model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.persistence.*;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
@Transactional
public class UserRepositoryImpl implements UserRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public User saveUser(User user) {
        try {
//            user.setId(2L);
            em.persist(user);
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }



//        if (user == null){
//            return null;
//        }
//
//        if (getUser(user.getId()) != null){
//            return null;
//        }
//
//        Connection conn = null;
//        try {
//            conn = DBUtil.getConnection();
//
//            PreparedStatement stmt = conn.prepareStatement("INSERT INTO users (username, password, email) VALUES (?, ?, ?)");
//            stmt.setString(1, user.getUsername());
//            stmt.setString(2, user.getPassword());
//            stmt.setString(3, user.getEmail());
//            stmt.execute();
//
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//        }

        return user;
    }


    @Override
    public List<User> getAllUsers() {

        List<User> users = new ArrayList<>();

//        EntityManagerFactory emf = DBUtil.getEntityManagerFactory();
//        EntityManager em = emf.createEntityManager();

//        Query query = em.createQuery("");
//        List<User> resultList = query.getResultList();
//        for (User u : resultList){
//            System.out.println(u.getUsername());
//        }

//        EntityManager em = DBUtil.getEntityManager();
        Query query = em.createQuery("SELECT u FROM User u");
        users = new ArrayList<User>(query.getResultList());

//        DBUtil.closeEntityManager(em);

//        Connection conn = null;
//        try {
//            conn = DBUtil.getConnection();
//
//            Statement stmt = conn.createStatement();
//            ResultSet rs = stmt.executeQuery("SELECT id, username, email, password FROM users");
//            while (rs.next()) {
//                users.add(DBUtil.fetchUserFromResultSet(rs));
//            }
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//        } finally {
//            DBUtil.closeConnection(conn);
//        }

        return users;
    }

    @Override
    public User getUser(Long id) {
        return em.find(User.class, id);

//        Connection conn = null;
//        User user = null;
//        try {
//            conn = DBUtil.getConnection();
//            PreparedStatement stmt = conn.prepareStatement("SELECT id, username, email, password FROM users WHERE id = ?");
//            stmt.setLong(1, id);
//            ResultSet rs = stmt.executeQuery();
//
//            if (rs.next()) {
//                user = DBUtil.fetchUserFromResultSet(rs);
//            }
//
//        } catch (SQLException | ClassNotFoundException e) {
//            e.printStackTrace();
//        } finally {
//            DBUtil.closeConnection(conn);
//        }

//        return user;
    }

    @Override
    public User getUserByUsername(String username) {
        TypedQuery<User> tq = em.createQuery("SELECT u FROM User u WHERE u.username = ?1", User.class);
        User user = null;
        try {
            user = tq.setParameter(1, username).getSingleResult();
        } catch (NoResultException e){
            e.printStackTrace();
        }
        return user;
//        return null;
    }


}
