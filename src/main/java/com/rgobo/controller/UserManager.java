package com.rgobo.controller;

import com.rgobo.model.User;
import com.rgobo.service.UserService;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

@Named
@SessionScoped
public class UserManager implements Serializable {

    @Inject
    UserService userService;

    private User currentUser;


    public String signIn(String username, String password) {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        User user = userService.getUserByUsername(username);

        if (user == null) {
            facesContext.addMessage("errorMessage", new FacesMessage(FacesMessage.SEVERITY_ERROR,"User doesn't exist", null));
            return "login";
        }

        if (!user.getPassword().equals(password)){
            facesContext.addMessage("errorMessage", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Incorrect password", null));
            return "login";
        }

        currentUser = user;
        return "home";
    }

    public String signOut() {
        // End the session, removing any session state, including the current user and content of the shopping cart
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

        // Redirect is necessary to let the browser make a new GET request
        return "home?faces-redirect=true";
    }

    public String save(User user) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        currentUser = userService.saveUser(user);
        if (currentUser == null){
//            facesContext.addMessage("errorMessage", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error saving user", null));
            return "user-details";
        }
        return "home";
    }

    public String openUserDetails(Long id){
        return "user-details?faces-redirect=true";
    }

    public boolean isSignedIn() {
        return currentUser != null;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(User currentUser) {
        this.currentUser = currentUser;
    }

    public List<User> getAllUsers(){
        return userService.getAllUsers();
    }

}
