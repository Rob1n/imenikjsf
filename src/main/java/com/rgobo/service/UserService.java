package com.rgobo.service;

import com.rgobo.model.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();

    User getUser(Long id);

    User getUserByUsername(String username);

    User saveUser(User user);

    User updateUser(Long id, User user);

    User deleteUser(Long id);
}
