package com.rgobo.service;

import com.rgobo.model.FileU;
import com.rgobo.repository.FileURepository;

import javax.faces.bean.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class FileUServiceImpl implements FileUService {

    @Inject
    private FileURepository fileURepository;

    @Override
    public List<FileU> getAllFileUs(){
        return fileURepository.getAllFileUs();
    }

    @Override
    public FileU getFileU(Long id){
        return fileURepository.getFileU(id);
    }

    @Override
    public FileU createFileU(FileU file){
        return fileURepository.createFileU(file);
    }

}
