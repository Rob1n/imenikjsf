package com.rgobo.service;

import com.rgobo.model.FileU;

import java.util.List;

public interface FileUService {
    List<FileU> getAllFileUs();

    FileU getFileU(Long id);

    FileU createFileU(FileU file);
}
