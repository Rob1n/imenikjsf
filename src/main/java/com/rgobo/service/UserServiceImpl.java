package com.rgobo.service;

import com.rgobo.model.User;
import com.rgobo.repository.UserRepository;

import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
@SessionScoped
public class UserServiceImpl implements UserService {

    @Inject
    private UserRepository userRepository;

//    private Map<String, User> userMap = new ConcurrentHashMap<>();

    @Override
    public List<User> getAllUsers() {
//        List<User> users = new ArrayList<>();
//        for (Map.Entry<String, User> user : userMap.entrySet()){
//            users.add(user.getValue());
//        }
//        return users;
        return userRepository.getAllUsers();
    }

    @Override
    public User getUser(Long id) {
        return userRepository.getUser(id);
    }

    @Override
    public User getUserByUsername(String username) {
//        return userMap.get(username);
        return userRepository.getUserByUsername(username);
    }

    @Override
    public User saveUser(User user) {
        return userRepository.saveUser(user);
//        userMap.put(user.getUsername(), user);
    }

    @Override
    public User updateUser(Long id, User user) {
        return null;
    }

    @Override
    public User deleteUser(Long id) {
        return null;
    }
}
