package com.rgobo.config;

import com.rgobo.model.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DBUtil {

    private static String dbUrl = "jdbc:mysql://localhost:3306/imenikJSF";
    private static String dbUser = "imenikUser";
    private static String dbPass = "password";

    public static Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(dbUrl, dbUser, dbPass);
    }

    public static void closeConnection(Connection conn) {
        if (conn != null){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static EntityManagerFactory getEntityManagerFactory() {
        Map<String, String> properties = new HashMap<String, String>();
        properties.put("javax.persistence.jdbc.user", "imenikUser");
        properties.put("javax.persistence.jdbc.password", "password");
        return Persistence.createEntityManagerFactory(
                "objectdb://localhost:6136/myDbFile.odb", properties);
    }

    public static EntityManager getEntityManager() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("LocalDB");
        return emf.createEntityManager();
    }

    public static void closeEntityManager(EntityManager em) {
        em.close();
    }

    public static User fetchUserFromResultSet(ResultSet rs) throws SQLException {
        return new User(Long.getLong(rs.getString("id")), rs.getString("username"), rs.getString("email"), rs.getString("password"));
    }



}
